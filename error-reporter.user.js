class ErrorReporter {
  static init() {
    try {
      window.addEventListener("load", (event) => {
        this.createContainer();
      });
    } catch (e) {
      console.error(e);
      alert(e);
    }
  }

  static createContainer() {
    try {
      this.frErrorMessageContainer = DOMHelpers.getElement({
        nodeName: "span"
      });

      const lrCloseButton = DOMHelpers.getElement({
        nodeName: "div",
        style: {
          position: "absolute",
          top: "0",
          right: "0",
          width: "20px",
          height: "20px",
          cursor: "pointer"
        },
        children: [{
          nodeName: "div",
          style: {
            width: "100%",
            height: "2px",
            position: "absolute",
            top: "50%",
            transform: "rotate(45deg)",
            "background-color": "black"
          }
        }, {
          nodeName: "div",
          style: {
            width: "100%",
            height: "2px",
            position: "absolute",
            top: "50%",
            transform: "rotate(-45deg)",
            "background-color": "black"
          }
        }]
      });

      this.frContainer = DOMHelpers.getElement({
        nodeName: "div",
        style: {
          position: "absolute",
          top: "20px",
          left: "0",
          right: "0",
          margin: "0 auto",
          background: "red",
          padding: "10px",
          width: "30%",
          "font-family": "-apple-system,Segoe UI,Tahoma,sans-serif",
          visibility: "hidden",
          zIndex: "1000000"
        },
        children: [{
            nodeName: "span",
            textContent: "An error occurred, see below."
          }, {
            nodeName: "br"
          },
          this.frErrorMessageContainer,
          lrCloseButton
        ]
      });

      lrCloseButton.addEventListener("click", (event) => {
        this.frContainer.style.visibility = "hidden";
      });

      document.body.appendChild(this.frContainer);
    } catch (e) {
      console.error(e);
      alert(e);
    }
  }

  static report(isError, ibTimeout) {
    try {
      if (this.frContainer) {
        this.frErrorMessageContainer.textContent = isError;
        this.frContainer.style.visibility = "visible";
        console.error(isError);
      } else {
        if (ibTimeout) {
          console.error(isError);
          alert(isError);
        } else {
          setTimeout(function() {
            ErrorReporter.report(isError, true);
          }, 1000);
        }
      }
    } catch (e) {
      console.error(e);
      alert(e);
    }
  }
}

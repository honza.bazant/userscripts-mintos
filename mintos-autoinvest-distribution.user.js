// ==UserScript==
// @name         mintos-autoinvest-distribution
// @namespace    mintos
// @version      1.7
// @require      dom-helper.user.js
// @require      error-reporter.user.js
// @description  Adds an option to set custom distribution for all companies
// @match        https://www.mintos.com/*
// @grant        none
// @noframes
// ==/UserScript==

/*jshint esversion: 8 */

(function() {
  'use strict';

  ErrorReporter.init();

  const wait = (millis) => new Promise(resolve => setTimeout(resolve, millis));

  function watchModalDisplayed(ibWasDisplayed, irCallback) {
    const lrModal = document.querySelector("div.diversification-modal");

    if (lrModal && !ibWasDisplayed) {
      irCallback(lrModal);
    }

    setTimeout(function() {
      watchModalDisplayed(lrModal != null, irCallback);
    }, 1000);
  }

  function waitForDiversificationButtons(irModal) {
    return new Promise((resolve, reject) => {
      const lrButton = irModal.querySelector("button.diversification-modal__distribution-button:first-of-type");
      resolve(lrButton);
    }).then(irButton => {
      if (!irButton) {
        console.debug("Diversification buttons not visible yet, wait");
        return wait(1000).then(() => waitForDiversificationButtons(irModal));
      } else {
        return irButton;
      }
    });
  }

  async function modalDisplayed(irModal) {
    try {
      console.info("Diversification modal displayed");

      const lrFirstButton = await waitForDiversificationButtons(irModal);

      // With the default maximum width the percent sign after the input is wrapped on the next line
      irModal.style.maxWidth = "130rem";

      const lrNewButton = DOMHelpers.getElement({
        nodeName: "a",
        className: "m-u-margin-left-4 m-u-margin-right-3 m-u-fs-6",
        textContent: "Set custom:"
      });

      const lrInput = DOMHelpers.getElement({
        nodeName: "input",
        type: "text",
        className: "m-u-margin-right-3 m-u-fs-6",
        style: {
          // Apply more or less the same styles as for other inputs in the modal.
          // The class system defining those styles is a bit complicated so it is easier to specify the rules explicitly.
          border: "1px solid rgba(0, 0, 0, 0.2)",
          borderRadius: "4px",
          outline: "none",
          textAlign: "center",
          width: "5.6rem"
        }
      });

      const lrPercentSign = DOMHelpers.getElement({
        nodeName: "span",
        textContent: "%",
        className: "m-u-fs-6"
      });

      lrNewButton.addEventListener("click", function(irEvent) {
        try {
          const lsNewValue = lrInput.value;
          const laInputs = irModal.querySelectorAll("span.diversity-rate__input input[type='text']");

          console.debug(`Set new value (${lsNewValue})`);

          Array.from(laInputs).forEach(irItem => {
            irItem.value = lsNewValue;
            // The "input" event must be fired, otherwise the form does not notice the change and will not allow the new values to be saved.
            // Additionally when user clicks to any of the inputs then the value will be changed to the old one.
            irItem.dispatchEvent(new Event("input"));
            console.debug("Value changed for one input");
          });

          console.debug("Done");
        } catch (e) {
          ErrorReporter.report(e);
        }
      });

      // Insert the elements before the first button ("Set default"). It would be more visually elegant at the end (the input would be aligned with the other inputs),
      // but when "Set equal" is clicked an orange warning is shown ("Set to equal can result in some of your strategy target not being invested...") which is inserted
      // right after that button, so our new controls will be below that warning and it looks ugly.
      lrFirstButton.parentNode.insertBefore(lrNewButton, lrFirstButton);
      lrFirstButton.parentNode.insertBefore(lrInput, lrFirstButton);
      lrFirstButton.parentNode.insertBefore(lrPercentSign, lrFirstButton);
    } catch(e) {
      ErrorReporter.report(e);
    }
  }

  try {
    watchModalDisplayed(false, modalDisplayed);
  } catch(e) {
    ErrorReporter.report(e);
  }
})();

class DOMHelpers {
  static getElement(irData) {
    let lrElement = document.createElement(irData.nodeName);

    for (const [lsProperty, lrValue] of Object.entries(irData)) {
      switch (lsProperty) {
        case "nodeName":
          break;
        case "dataset":
          for (const [lsDatasetKey, lrDatasetValue] of Object.entries(lrValue)) {
            lrElement.dataset[lsDatasetKey] = lrDatasetValue;
          }
          break;
        case "style":
          for (const [lsStyleKey, lrStyleValue] of Object.entries(lrValue)) {
            lrElement.style[lsStyleKey] = lrStyleValue;
          }
          break;
        case "children":
          for (const lrChild of lrValue) {
            lrElement.appendChild(lrChild instanceof Node ? lrChild : DOMHelpers.getElement(lrChild));
          }
          break;
        default:
          lrElement[lsProperty] = lrValue;
      }
    }

    return lrElement;
  }

  static insertAsFirstChild(isQuerySelector, irNode) {
    const lrTarget = document.querySelector(isQuerySelector);

    if (lrTarget) {
      lrTarget.insertBefore(irNode, lrTarget.firstChild);
    } else {
      throw `Target element not found: ${isQuerySelector}`;
    }
  }

  static insertBefore(isQuerySelector, irNode) {
    const lrTarget = document.querySelector(isQuerySelector);

    if (lrTarget) {
      lrTarget.parentNode.insertBefore(irNode, lrTarget);
    } else {
      throw `Target element not found: ${isQuerySelector}`;
    }
  }

  static insertAfter(isQuerySelector, ...iaNodes) {
    const lrTarget = document.querySelector(isQuerySelector);

    if (lrTarget) {
      iaNodes.reverse().forEach(irNode => lrTarget.parentNode.insertBefore(irNode, lrTarget.nextSibling));
    } else {
      throw `Target element not found: ${isQuerySelector}`;
    }
  }
}

function MyException(isMessage, isSource) {
  this.fsMessage = isMessage;
  this.fsSource = isSource;
}

var grDOMHelper = new function() {

  this.CreateElementByPattern = function(isPattern) {
    var laItems = isPattern.split("/");
    var lrPreviousElement, lrCurrentElement;
    var lsTagName, lsAttrDef;
    var lnAttrDefStart, lnAttrDefEnd;
    var lrRetVal = {First: null, Last: null};

    for(var i = 0; i < laItems.length; i++) {
      lsTagName = laItems[i];
      lsAttrDef = "";

      if((lnAttrDefStart = lsTagName.indexOf("[")) != -1) {
        lnAttrDefEnd =  lsTagName.indexOf("]");

        if(lnAttrDefEnd == -1)
          throw new MyException("Pattern error: attribute definition not closed by ]", "CreateElementByPattern");
        else if(lnAttrDefEnd != lsTagName.length - 1)
          throw new MyException("Pattern error: attribute definition closing bracket not at the end", "CreateElementByPattern");

        lsAttrDef = lsTagName.slice(lnAttrDefStart + 1, lnAttrDefEnd);
        lsTagName = lsTagName.slice(0, lnAttrDefStart);
      }

      lrCurrentElement = document.createElement(lsTagName);
      this.ApplyAttributesToElement(lrCurrentElement, lsAttrDef);

      if(i == 0) {
        lrRetVal.First = lrCurrentElement;
      } else {
        lrPreviousElement.appendChild(lrCurrentElement);
      }

      lrPreviousElement = lrCurrentElement;
      lrRetVal.Last = lrCurrentElement;
    }

    return lrRetVal;
  }

  this.GetElementByPattern = function(isPattern) {
    var lrSplitted = isPattern.split("/");
    var lrTmp;
    var lsChild, lsTagName, lsInsertMethod, lsPosition;
    var lnPosition, lnCount;
    var lrCurrentElement = null;
    var lrNextElement;
    var lrChildren;
    var lbFindLastElement;
    var lrLastChild;

    if(lrSplitted[0].charAt(0) == "#") {
      // začínáme od nějakého elementu podle ID
      var lsID = lrSplitted[0].substr(1);
      lrCurrentElement = document.getElementById(lsID);

      if(lrCurrentElement == null) {
        throw new MyException("Element with ID: " + lsID + " not found", "GetElementByPattern");
      }
    } else if(lrSplitted[0].charAt(0) == ".") {
      var lsClassName = lrSplitted[0].substr(1);
      var lrAllElements = document.getElementsByClassName(lsClassName);

      if(lrAllElements.length == 0) {
        throw new MyException("Element with class: " + lsClassName + " not found", "GetElementByPattern");
      }

      // nápad: vracet kolekci elementů místo toho prvního
      lrCurrentElement = lrAllElements[0];
    } else {
      if(lrSplitted[0].toLowerCase() != "html") {
        throw new MyException("Insert pattern must start with html", "GetElementByPattern");
      }

      lrCurrentElement = document.documentElement;
    }

    for(var i = 1; i < lrSplitted.length; i++) { // NOTE: začínám od 1, protože kořenový prvek už jsem zpracoval
      lsChild = lrSplitted[i];

      // nemáme zadanou pozici -> implicitně budeme předpokládat 1
      if(lsChild.indexOf("[") == -1 || lsChild.indexOf("]") == -1) {
        // musíme dát pozor v případech vkládacách vzorů, kdy u posledního prvku je za tagem ještě ":" a specifikátor poizce
        var lnColonIndex = lsChild.indexOf(":");

        if(lnColonIndex == -1)
          lsChild = lsChild + "[1]";
        else
          lsChild = lsChild.substring(0, lnColonIndex) + "[1]" + lsChild.substring(lnColonIndex);
      }

      lsTagName = lsChild.slice(0, lsChild.indexOf("[")).toLowerCase();
      lsPosition = lsChild.slice(lsChild.indexOf("[") + 1, lsChild.indexOf("]"));

      if(lsPosition == "<last>") {
        lbFindLastElement = true;
        lnPosition = -1;
      } else {
        lnPosition = parseInt(lsPosition);
        lbFindLastElement = false;
      }

      if(isNaN(lnPosition))
        throw new MyException("Position is not a valid number", "GetElementByPattern");

      lrChildren = lrCurrentElement.childNodes;
      lnCount = 0;

      lrNextElement = null;
      lrLastChild = null;

      for(var j = 0; j < lrChildren.length; j++) {
        if(lsTagName == "*") {
          if(lrChildren[j].tagName !== undefined) {
            lnCount++;
            lrLastChild = lrChildren[j];
          }
        } else {
          if(lrChildren[j].tagName !== undefined && lrChildren[j].tagName.toLowerCase() == lsTagName) {
            lnCount++;
            lrLastChild = lrChildren[j];
          }
        }

        if(lnCount == lnPosition) { // pro případ <last> je toto v pořádku, lnPosition je v takovém případě -1, ale lnCount startuje od 0
          lrNextElement = lrChildren[j];
          break;
        }
      }

      if(lbFindLastElement && lrLastChild == null)
        throw new MyException("Element does not have any children with tag name '" + lsTagName + "'", "GetElementByPattern");
      if(!lbFindLastElement && lrNextElement == null)
        throw new MyException("Element does not have a child with tag name '" + lsTagName + "' number " + lnPosition, "GetElementByPattern");

      if(lbFindLastElement)
        lrCurrentElement = lrLastChild;
      else
        lrCurrentElement = lrNextElement;
    }

    return lrCurrentElement;
  }

  this.InsertElementByPattern = function(irElement, isPattern) {
    if(isPattern.length == 0)
      return;

    var lrCurrentElement = this.GetElementByPattern(isPattern);
    var laSplitted = isPattern.split("/");
    var lsLastPiece = laSplitted[laSplitted.length - 1];

    if(lsLastPiece.indexOf(":") == -1)
      throw new MyException("Insert metod must be specified", "InsertElementByPattern");

    var lsInsertMethod = lsLastPiece.substr(lsLastPiece.indexOf(":") + 1);

    switch(lsInsertMethod) {
      case "before":
        lrCurrentElement.parentNode.insertBefore(irElement, lrCurrentElement);
        break;
      case "after":

        if(lrCurrentElement.nextSibling == null) {
          lrCurrentElement.parentNode.appendChild(irElement);
        } else {
          lrCurrentElement.parentNode.insertBefore(irElement, lrCurrentElement.nextSibling);
        }

        break;
      case "at-end":
        lrCurrentElement.appendChild(irElement);
        break;

      default:
        throw new MyException("Invalid insert method: " + lsInsertMethod, "InsertElementByPattern");
    }
  }

  /**
   *  Zjistí, zda zadaný element má nastavenou zadanou třídu.
   *
   *  @param    irElement     element
   *  @param    isClassName   třída
   *  @return   - pokud je irElement undefined nebo null, pak false
   *            - pokud element obsahuje třídu isClassName, pak true
   *            - jinak false
   */
  this.HasClass = function(irElement, isClassName) {
    if(irElement == undefined || irElement == null)
      return false;

    var lsClassNames = irElement.className;
    var laClassNamesSplitted = lsClassNames.split(" ");

    for(var j = 0; j < laClassNamesSplitted.length; ++j) {
      if(laClassNamesSplitted[j] == isClassName)
        return true;
    }

    return false;
  }

  // INTERNAL FUNCTIONS

  this.ApplyAttributesToElement = function(orElement, isAttributeDefinition) {
    try {
      if(isAttributeDefinition.length == 0)
        return orElement;

      var lrAttributes = isAttributeDefinition.split(",");
      var lsAttrName, lsAttrValue;
      var lrSplitted;

      for(var i = 0; i < lrAttributes.length; i++) {
        lrSplitted = lrAttributes[i].split("=");
        lsAttrName = lrSplitted[0];
        lsAttrValue = lrSplitted[1];

        if(lsAttrName.toLowerCase() == "style") {
          this.ApplyStyleToElement(orElement, lsAttrValue);
        } else {
          this.ApplyOrdinaryAttributeToElement(orElement, lsAttrName, lsAttrValue);
        }
      }

      return orElement;
    } catch(e) {
      this.ReportError(e);
    }
  }

  this.ApplyOrdinaryAttributeToElement = function(orElement, isAttrName, isAttrValue) {
    orElement.setAttribute(isAttrName, isAttrValue);
    return orElement;
  }

  this.ApplyStyleToElement = function(orElement, isStyleDefinition) {
    var lrProperties = isStyleDefinition.split(";");
    var lsPropName, lsPropValue;
    var lrSplitted;

    for(var i = 0; i < lrProperties.length; i++) {
      lrSplitted = lrProperties[i].split(":");
      lsPropName = lrSplitted[0];
      lsPropValue = lrSplitted[1];
      orElement.style.setProperty(lsPropName, lsPropValue, "");
    }

    return orElement;
  }

}

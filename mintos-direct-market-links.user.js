// ==UserScript==
// @name        mintos-direct-market-links
// @namespace   mintos
// @version     1.9
// @require     dom-helper.user.js
// @require     error-reporter.user.js
// @description Adds direct links to Primary and Secondary market on Mintos
// @match       https://www.mintos.com/**
// @grant       none
// @noframes
// ==/UserScript==

/*jshint esversion: 8 */

(function() {
  'use strict';

  ErrorReporter.init();

  const wait = (millis) => new Promise(resolve => setTimeout(resolve, millis));

  const PRIMARY_MARKET_LINK_ID = "mintos-helper-primary-market-link";
  const SECONDARY_MARKET_LINK_ID = "mintos-helper-secondary-market-link";

  async function checkLinksMissing(irCallback) {
    const lrHeader = document.querySelector("div.header");

    if (lrHeader) {
      const lrPrimaryMarketLink = lrHeader.querySelector(`#${PRIMARY_MARKET_LINK_ID}`);
      const lrSecondaryMarketLink = lrHeader.querySelector(`#${SECONDARY_MARKET_LINK_ID}`);

      if (!lrPrimaryMarketLink && !lrSecondaryMarketLink) {
        console.info("Links are not present, add them");

        // Mintos itself seems to be doing something with the menu.
        // Without the timeout sometimes a strange thing happens (noticed only in Chrome): when the links are added, their texts are changed to "Portfolio" and "Help" almost immediately.
        // Also some strange error is written to console.
        // Delaying the insertion seems to prevent it.
        await wait(1000).then(() => irCallback(lrHeader));
      }
    } else {
      console.debug("Header is not visible yet");
    }

    setTimeout(function() {
      try {
        checkLinksMissing(irCallback);
      } catch (e) {
        ErrorReporter.report(e);
      }
    }, 1000);
  }

  /**
   * Adds links to primary and secondary markets to the top navigation
   */
  function addPrimarySecondaryMarketLinks(irHeader) {
    const lrPortfolioLink = irHeader.querySelector("div.header-actions__links div div:nth-child(3)");

    if (!lrPortfolioLink) {
      console.warn("Invest link not found");
    } else {
      const lrPrimaryMarketLink = DOMHelpers.getElement({
        nodeName: "div",
        className: "nav-item",
        id: PRIMARY_MARKET_LINK_ID,
        children: [{
          nodeName: "a",
          href: "https://www.mintos.com/en/set-of-notes/primary/",
          target: "_parent",
          className: "m-u-fw-4 m-u-padding-right--xl-5 m-u-color-1-75--text m-u-color-b7--text:hover",
          textContent: "Primary market",
          style: {
            whiteSpace: "nowrap"
          }
        }]
      });

      const lrSecondaryMarketLink = DOMHelpers.getElement({
        nodeName: "div",
        className: "nav-item",
        id: SECONDARY_MARKET_LINK_ID,
        children: [{
          nodeName: "a",
          href: "https://www.mintos.com/en/set-of-notes/secondary/",
          target: "_parent",
          className: "m-u-fw-4 m-u-padding-right--xl-5 m-u-color-1-75--text m-u-color-b7--text:hover",
          textContent: "Secondary market",
          style: {
            whiteSpace: "nowrap"
          }
        }]
      });

      lrPortfolioLink.parentNode.insertBefore(lrPrimaryMarketLink, lrPortfolioLink);
      lrPortfolioLink.parentNode.insertBefore(lrSecondaryMarketLink, lrPortfolioLink);

      irHeader.style.maxWidth = "1800px";
    }
  }

  try {
    checkLinksMissing(addPrimarySecondaryMarketLinks);
  } catch (e) {
    ErrorReporter.report(e);
  }
})();

// ==UserScript==
// @name        mintos-balance-stripe
// @namespace   mintos
// @version     1.7
// @require     dom-helper.user.js
// @require     error-reporter.user.js
// @description Makes whole Mintos balance stripe be visible
// @match       https://www.mintos.com/**
// @grant       none
// @noframes
// ==/UserScript==

/*jshint esversion: 8 */

(function() {
  'use strict';

  ErrorReporter.init();

  const wait = (millis) => new Promise(resolve => setTimeout(resolve, millis));

  const GRADIENT_CLASS_NAME = "currencies__gradient";

  async function checkBalanceHidden(irCallback) {
    const lrBalanceMenu = document.querySelector("div.balance-card");

    if (lrBalanceMenu) {
      const lrGradient = lrBalanceMenu.querySelector(`div.${GRADIENT_CLASS_NAME}`);

      if (lrGradient) {
        console.info("Gradient element hiding whole balance is present");

        // Mintos must be doing some black magic, because sometimes the available balance stripe is restored to its original state shortly after being modified by this script (noticed only in Chrome).
        // Adding the wait seems to prevent it.
        await wait(1000).then(() => irCallback(lrBalanceMenu));
      }
    }

    setTimeout(function() {
      try {
        checkBalanceHidden(irCallback);
      } catch (e) {
        ErrorReporter.report(e);
      }
    }, 1000);
  }

  /**
   * Changes the style of the available balance stripe at the top of the page so that it is fully visible
   */
  function changeAvailableBalanceStyle(irBalanceMenu) {
    const lrGradient = irBalanceMenu.querySelector(`div.${GRADIENT_CLASS_NAME}`);
    const lrAvailableBalanceDiv = irBalanceMenu.querySelector("div.available-balance");

    if (lrGradient) {
      lrGradient.parentNode.removeChild(lrGradient);
    }

    if (lrAvailableBalanceDiv) {
      lrAvailableBalanceDiv.style.maxWidth = "100rem";
    }
  }

  try {
    checkBalanceHidden(changeAvailableBalanceStyle);
  } catch (e) {
    ErrorReporter.report(e);
  }
})();

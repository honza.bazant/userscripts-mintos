# Userscripts for Mintos

## What?
Here you can find some useful userscripts for [Mintos](https://mintos.com), a P2P lending marketplace.
Userscripts are scripts written in Javascript which allow one to extend functionality of any webpage. All you need
is an userscript manager, for example [Greasemonkey](https://addons.mozilla.org/cs/firefox/addon/greasemonkey/)
or [Tampermonkey](https://www.tampermonkey.net/). All the scripts were tested in Firefox 88.0 with Greasemonkey and
Tampermonkey and in Chrome 90.0 with Tampermonkey.

## Okay, so what's included?

### Set custom distribution for all companies at once
Do you have an automated strategy with tens of companies and want to set the same custom distribution for all of
them? Yes? So you are most certainly tired of pasting or even typing the same number in all the fields, right?
I have good news for you - you can now do so extremely fast! Look at the following
picture:

![Diversification settings modal dialog](doc/automated-strategy-custom-distribution.png)

Did you notice the new "Set custom" button and the input field next to it, right?
Yes, it is that easy - just type the desired distribution in that field, click the "Set custom" button and that's
it!

To get this useful feature, install the script named `mintos-autoinvest-distribution.user.js` above.
The fastest way to do so is using [this direct link](https://gitlab.com/honza.bazant/userscripts-mintos/-/raw/master/mintos-autoinvest-distribution.user.js).
When you click the link, a dialog of your userscript manager should open asking you if you really want to install
the script.

### Visible balances of all currencies
If you have more than two currencies, you may have noticed that you see the balances of only the first two
currencies in the upper right corner, the rest are hidden, and you must click the balance widget to see them.
Pretty annoying, given that your screen is wide enough, right? I have a solution for this too! See the pictures.

Before:

![Balance widget before](doc/balance-before.png)

After:

![Balance widget after](doc/balance-after.png)

This feature is provided by script named `mintos-balance-stripe.user.js`.
[Direct link here.](https://gitlab.com/honza.bazant/userscripts-mintos/-/raw/master/mintos-balance-stripe.user.js)

### Direct links to markets
Do you often invest manually? Yes? So when you want to open let's say the secondary market, you have to first
click "Invest" link in the top menu, then "Available loans" link and finally switch to the secondary market. Did
you ever say to yourself "What? Three clicks? Are they serious?" ? Yes, me too, so here you have a script that
adds direct links to primary and secondary markets to the main menu.
See it in action:

![Main menu with direct market links](doc/direct-market-links.png)

This feature is provided by script named `mintos-direct-market-links.user.js`.
[Direct link here.](https://gitlab.com/honza.bazant/userscripts-mintos/-/raw/master/mintos-direct-market-links.user.js)
